package main

import (
	"strings"
	"log"
	"os"
	"net/url"
	"net/http"
	"io"
	"bytes"
	"encoding/json"
	"fmt"
	"sync"
	"sort"
	"bufio"
)

const (
	API_KEY  = "trnsl.1.1.20180523T112547Z.725c1553f076f23f.81ed61b7dafaf2282289001215c8fe8a4ddbc894"
	BASE_URL = "https://translate.yandex.net"
)

var (
	Debug   = log.New(os.Stdout, "[Debug] ", log.Ldate|log.Ltime|log.Lshortfile)
	Info    = log.New(os.Stdout, "[Info] ", log.Ldate|log.Ltime|log.Lshortfile)
	Warning = log.New(os.Stdout, "[Warning] ", log.Ldate|log.Ltime|log.Lshortfile)
	Error   = log.New(os.Stdout, "[Error] ", log.Ldate|log.Ltime|log.Lshortfile)

	m = make(TranslateMap)
	mapMutex = sync.RWMutex{}

	wg sync.WaitGroup
)

type Client struct {
	BaseURL   *url.URL
	UserAgent string

	httpClient *http.Client
}

type Translate struct {
	LanguageDetected string   `json:"lang"`
	Translation      []string `json:"text"`
	Word             string
	TranslateTo      string
}

type TranslateMap map[int]*Translate

func NewClient(httpClient *http.Client) *Client {
	if httpClient == nil {
		httpClient = http.DefaultClient
	}
	baseURL, _ := url.Parse(BASE_URL)

	c := &Client{httpClient: httpClient, BaseURL: baseURL, UserAgent: "TEST"}
	return c
}

func (c *Client) DetectLanguage(word string, i int) {
	defer wg.Done()
	req, err := c.newRequest("GET", "/api/v1.5/tr.json/detect", nil)

	q := req.URL.Query()
	q.Add("text", word)
	req.URL.RawQuery = q.Encode()

	if err != nil {
		Error.Println(err)
	}

	var t Translate
	_, err = c.do(req, &t)

	if err != nil {
		Error.Println(err)
	}

	translated := &Translate{Word: word, LanguageDetected: t.LanguageDetected}
	mapMutex.Lock()
	m[i] = translated
	mapMutex.Unlock()
}

func (c *Client) TranslateWord(word string, lang string, i int) {
	defer wg.Done()
	req, err := c.newRequest("GET", "/api/v1.5/tr.json/translate", nil)

	q := req.URL.Query()
	q.Add("text", word)
	q.Add("lang", lang)
	req.URL.RawQuery = q.Encode()

	if err != nil {
		Error.Println(err)
	}

	var t Translate
	_, err = c.do(req, &t)

	if err != nil {
		Error.Println(err)
	}

	mapMutex.Lock()
	m[i].Translation = t.Translation
	mapMutex.Unlock()
}

func (c *Client) newRequest(method, path string, body interface{}) (*http.Request, error) {
	rel := &url.URL{Path: path}
	u := c.BaseURL.ResolveReference(rel)
	var buf io.ReadWriter
	if body != nil {
		buf = new(bytes.Buffer)
		err := json.NewEncoder(buf).Encode(body)
		if err != nil {
			return nil, err
		}
	}
	req, err := http.NewRequest(method, u.String(), buf)
	if err != nil {
		return nil, err
	}
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Accept", "application/json")
	req.Header.Set("User-Agent", c.UserAgent)
	q := req.URL.Query()
	q.Add("key", API_KEY)
	req.URL.RawQuery = q.Encode()
	return req, nil
}

func (c *Client) do(req *http.Request, v interface{}) (*http.Response, error) {
	resp, err := c.httpClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	err = json.NewDecoder(resp.Body).Decode(v)
	return resp, err
}

func main() {

	//Read string
	reader := bufio.NewReader(os.Stdin)
	fmt.Print("Enter text: ")
	text, _ := reader.ReadString('\n')

	//Parse string
	var ss = strings.Split(text, " ")
	//var ss = strings.Split("green яблоко mandorla buono amigo arividerci котлета traduttore", " ")

	client := NewClient(nil)

	//Detect language
	for index, word := range ss {
		wg.Add(1)
		go client.DetectLanguage(word, index)
	}

	wg.Wait() // Wait for all the words have been added and language detected

	//Translate each word
	for index, t := range m {
		switch t.LanguageDetected {
		case "ru":
			continue
		case "en":
			t.TranslateTo = "en-ru"
		default:
			t.TranslateTo = t.LanguageDetected + "-en"
		}

		wg.Add(1)
		go client.TranslateWord(t.Word, t.TranslateTo, index)
	}

	wg.Wait() //Wait for all the words have been translated

	var keys []int
	for k := range m {
		keys = append(keys, k)
	}
	sort.Ints(keys)

	for i := range keys {
		if len(m[i].Translation) == 0 {
			fmt.Printf("%s ", m[i].Word)
		} else if i == len(m)-1 {
			fmt.Printf("%s", m[i].Translation[0])
		} else {
			fmt.Printf("%s ", m[i].Translation[0])
		}
	}
}
